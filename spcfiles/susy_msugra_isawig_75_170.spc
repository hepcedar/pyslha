#=====================================================================
#  ISAJET SUSY parameters in Les Houches Accord format
#  Created by isa2lha.py (external script)
#             Version 1, March 12, 2010, b.k.gjelsten@fys.uio.no
#             The initial purpose of the script was to allow for output of 
#             isasusy.x to be interfaced to Prospino
#             (For bugs/feedback, please send an email)
#
#              The wig-file is the basis for the spectrum and mixing matrices
#              Optionally the out-file can be read as well (so far only implemented
#              for isasusy.x output), giving some extra info in the slha output
#              From the wig file only values at the low scale are available, 
#              hence 'General MSSM Simulation' is given as MODSEL
#===================================================================== SPINFO
Block SPINFO   # Program information
     1   From wig file of ISAJET        # Spectrum Calculator
#===================================================================== MODSEL
Block MODSEL   # Model selection
     1     0   # General MSSM Simulation
#===================================================================== SMINPUTS
Block SMINPUTS   # Standard Model inputs
     6     1.72500000e+02   # m_{top}(pole)
#===================================================================== MINPAR
Block MINPAR   # SUSY breaking input parameters
     3     3.00000000e+00   # tan(beta)
#===================================================================== MASS
Block MASS   # Scalar and gaugino mass spectrum
# PDG code     mass               particle
        25     9.28581000e+01   # h
        35     3.15304100e+02   # H
        36     3.20031400e+02   # H+
        37     3.08164600e+02   # A
   1000001     4.04106600e+02   # dL
   1000002     3.97179100e+02   # uL
   1000003     4.04106700e+02   # sL
   1000004     3.97181600e+02   # cL
   1000005     3.70784300e+02   # b1
   1000006     2.63194600e+02   # t1
   2000001     3.87953700e+02   # dR
   2000002     3.87205600e+02   # uR
   2000003     3.87953800e+02   # sR
   2000004     3.87208200e+02   # cR
   2000005     3.88361600e+02   # b2
   2000006     4.49629300e+02   # t2
   1000011     1.44659200e+02   # eL
   1000013     1.44659300e+02   # muL
   1000015     1.04456000e+02   # ta1
   2000011     1.04921400e+02   # eR
   2000013     1.04921400e+02   # muR
   2000015     1.44605800e+02   # ta2
   1000012     1.24660900e+02   # snue
   1000014     1.24660900e+02   # snum
   1000016     1.23780100e+02   # snut
   1000021     4.29479600e+02   # gl
   1000022     5.83570000e+01   # N1
   1000023     1.07417700e+02   # N2
   1000025    -2.64731300e+02   # N3
   1000035     2.99801000e+02   # N4
   1000024     1.03798700e+02   # C1
   1000037     2.94527800e+02   # C2
#===================================================================== NMIX
Block NMIX   # neutralino mixing matrix
  1  1     9.43217580e-01   # 
  1  2    -2.03794690e-01   # 
  1  3     2.32542460e-01   # 
  1  4    -1.21376130e-01   # 
  2  1    -2.87832290e-01   # 
  2  2    -8.94628940e-01   # 
  2  3     2.85057370e-01   # 
  2  4    -1.88504410e-01   # 
  3  1     5.95960800e-02   # 
  3  2    -8.95265200e-02   # 
  3  3    -6.92517460e-01   # 
  3  4    -7.13339750e-01   # 
  4  1     1.54729780e-01   # 
  4  2    -3.87416720e-01   # 
  4  3    -6.20553430e-01   # 
  4  4     6.63988410e-01   # 
#===================================================================== UMIX
Block UMIX   # chargino U mixing matrix
  1  1    -8.78017190e-01   # Umix_{11}
  1  2     4.78629140e-01   # Umix_{12}
  2  1    -4.78629140e-01   # Umix_{21}
  2  2    -8.78017190e-01   # Umix_{22}
#===================================================================== UMIX
Block VMIX   # chargino V mixing matrix
  1  1    -9.47550120e-01   # Vmix_{11}
  1  2     3.19607200e-01   # Vmix_{12}
  2  1    -3.19607200e-01   # Vmix_{21}
  2  2    -9.47550120e-01   # Vmix_{22}
#===================================================================== STOPMIX
Block STOPMIX   # stop mixing matrix
  1  1     6.03740730e-01   # O_{11}
  1  2     7.97180739e-01   # O_{12}
  2  1    -7.97180739e-01   # O_{21}
  2  2     6.03740730e-01   # O_{22}
#===================================================================== SBOTMIX
Block SBOTMIX   # sbottom mixing matrix
  1  1     9.66227869e-01   # O_{11}
  1  2     2.57689162e-01   # O_{12}
  2  1    -2.57689162e-01   # O_{21}
  2  2     9.66227869e-01   # O_{22}
#===================================================================== STAUMIX
Block STAUMIX   # stau mixing matrix
  1  1     1.57472640e-01   # O_{11}
  1  2     9.87523350e-01   # O_{12}
  2  1    -9.87523350e-01   # O_{21}
  2  2     1.57472640e-01   # O_{22}
#===================================================================== ALPHA
Block ALPHA   # Effective Higgs mixing parameter
          -3.72716550e-01   # alpha_h
#===================================================================== HMIX
Block HMIX (Q = -1.00000)   # Higgs mixing parameters
     1     2.61659851e+02   # mu(Q)
     4     9.49654207e+04   # m_A^2(Q)
#===================================================================== AU
Block AU (Q = -1.00000)   #
  1  1    -3.13666229e+02   # A_u
  2  2    -3.13666229e+02   # A_c
  3  3    -3.13666229e+02   # A_t
#===================================================================== AD
Block AD (Q = -1.00000)   #
  1  1    -5.03861908e+02   # A_d
  2  2    -5.03861908e+02   # A_s
  3  3    -5.03861908e+02   # A_b
#===================================================================== AE
Block AE (Q = -1.00000)   #
  1  1    -1.09110580e+02   # A_e
  2  2    -1.09110580e+02   # A_mu
  3  3    -1.09110580e+02   # A_tau
