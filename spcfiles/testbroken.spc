# SUSY Les Houches Accord 2 - MSSM spectrum + Decays
# SPheno v3.2.5be
# W. Porod, Comput. Phys. Commun. 153 (2003) 275-315, hep-ph/0301101
# in case of problems send email to porod@physik.uni-wuerzburg.de
# Created: 08.10.2013,  19:18
Block SPINFO         # Program information
     1   SPheno      # spectrum calculator
     2   v3.2.5be    # version number
#
Block SPhenoINFO     # SPheno specific information
    1      2         # using 2-loop RGEs
    2      1         # using running masses for boundary conditions at mZ
# Either the general MSSM or a model has been used
# which has not yet been implemented in the LesHouches standard
Block MINPAR  # Input parameters
    3    1.00000000E+01  # tanb at m_Z    
    4    1.00000000E+00  # Sign(mu)
Block EXTPAR  # non-universal input parameters
    1    1.00000000E+03  # M_1
    2    1.00000000E+03  # M_2
    3    1.00000000E+03  # M_3
   11    2.00000000E+03  # A_t
   12    0.00000000E+00  # A_b
   13    0.00000000E+00  # A_tau
   23    1.00000000E+02  # mu
   26    1.00000000E+03  # m_A, pole mass
   31    5.00000000E+03  # M^2_L11
   32    5.00000000E+03  # M^2_L22
   33    5.00000000E+03  # M^2_L33
   34    5.00000000E+03  # M^2_E11
   35    5.00000000E+03  # M^2_E22
   36    5.00000000E+03  # M^2_E33
   41    5.00000000E+03  # M^2_Q11
   42    5.00000000E+03  # M^2_Q22
   43    5.00000000E+02  # M^2_Q33
   44    5.00000000E+03  # M^2_U11
   45    5.00000000E+03  # M^2_U22
   46    1.20000000E+03  # M^2_U33
   47    5.00000000E+03  # M^2_D11
   48    5.00000000E+03  # M^2_D22
   49    1.50000000E+03  # M^2_D33
Block SMINPUTS  # SM parameters
         1     1.27908953E+02  # alpha_em^-1(MZ)^MSbar
         2     1.16637000E-05  # G_mu [GeV^-2]
         3     1.18400000E-01  # alpha_s(MZ)^MSbar
         4     9.11876000E+01  # m_Z(pole)
         5     4.19000000E+00  # m_b(m_b), MSbar
         6     1.73300000E+02  # m_t(pole)
         7     1.77700000E+00  # m_tau(pole)
         8     0.00000000E+00  # m_nu_3
        11     5.10998910E-04  # m_e(pole)
        12     0.00000000E+00  # m_nu_1
        13     1.05658000E-01  # m_muon(pole)
        14     0.00000000E+00  # m_nu_2
        21     5.00000000E-03  # m_d(2 GeV), MSbar
        22     3.00000000E-03  # m_u(2 GeV), MSbar
        23     1.05000000E-01  # m_s(2 GeV), MSbar
        24     1.27000000E+00  # m_c(m_c), MSbar
Block gauge Q=  7.67747601E+02  # (SUSY scale)
   1    3.60216963E-01  # g'(Q)^DRbar
   2    6.35981602E-01  # g(Q)^DRbar
   3    1.05550192E+00  # g3(Q)^DRbar
Block Yu Q=  7.67747601E+02  # (SUSY scale)
  1  1     8.58301083E-06   # Y_u(Q)^DRbar
  2  2     3.63347324E-03   # Y_c(Q)^DRbar
  3  3     1.01545554E+00   # Y_t(Q)^DRbar
Block Yd Q=  7.67747601E+02  # (SUSY scale)
  1  1     1.38858142E-04   # Y_d(Q)^DRbar
  2  2     2.91601908E-03   # Y_s(Q)^DRbar
  3  3     1.67880005E-01   # Y_b(Q)^DRbar
Block Ye Q=  7.67747601E+02  # (SUSY scale)
  1  1     2.86832859E-05   # Y_e(Q)^DRbar
  2  2     5.93076899E-03   # Y_mu(Q)^DRbar
  3  3     9.97266123E-02   # Y_tau(Q)^DRbar
Block Au Q=  7.67747601E+02  # (SUSY scale)
  1  1     0.00000000E+00   # A_u(Q)^DRbar
  2  2     0.00000000E+00   # A_c(Q)^DRbar
  3  3     1.79405656E+03   # A_t(Q)^DRbar
Block Ad Q=  7.67747601E+02  # (SUSY scale)
  1  1     0.00000000E+00   # A_d(Q)^DRbar
  2  2     0.00000000E+00   # A_s(Q)^DRbar
  3  3     0.00000000E+00   # A_b(Q)^DRbar
Block Ae Q=  7.67747601E+02  # (SUSY scale)
  1  1     0.00000000E+00   # A_e(Q)^DRbar
  2  2     0.00000000E+00   # A_mu(Q)^DRbar
  3  3     0.00000000E+00   # A_tau(Q)^DRbar
Block MSOFT Q=  7.67747601E+02  # soft SUSY breaking masses at Q
   1    1.00000000E+03  # M_1
   2    1.00000000E+03  # M_2
   3    1.00000000E+03  # M_3
  21    9.65634970E+05  # M^2_(H,d)
  22   -2.32252966E+04  # M^2_(H,u)
  31    5.00000000E+03  # M_(L,11)
  32    5.00000000E+03  # M_(L,22)
  33    5.00000000E+03  # M_(L,33)
  34    5.00000000E+03  # M_(E,11)
  35    5.00000000E+03  # M_(E,22)
  36    5.00000000E+03  # M_(E,33)
  41    5.00000000E+03  # M_(Q,11)
  42    5.00000000E+03  # M_(Q,22)
  43    5.00000000E+02  # M_(Q,33)
  44    5.00000000E+03  # M_(U,11)
  45    5.00000000E+03  # M_(U,22)
  46    1.20000000E+03  # M_(U,33)
  47    5.00000000E+03  # M_(D,11)
  48    5.00000000E+03  # M_(D,22)
  49    1.50000000E+03  # M_(D,33)
Block MASS  # Mass spectrum
#   PDG code      mass          particle
         6     1.73300000E+02  # m_t(pole)
        23     9.11876000E+01  # m_Z(pole)
        24     8.03319093E+01  # W+
        15     1.77700000E+00  # m_tau(pole)
        25     1.25500000E+02  # h0
        35     1.00011213E+03  # H0
        36     1.00000000E+03  # A0
        37     1.00336488E+03  # H+
   1000001     5.05432773E+03  # ~d_L
   2000001     5.04488479E+03  # ~d_R
   1000002     5.05385903E+03  # ~u_L
   2000002     5.04603791E+03  # ~u_R
   1000003     5.05432958E+03  # ~s_L
   2000003     5.04488617E+03  # ~s_R
   1000004     5.05386091E+03  # ~c_L
   2000004     5.04604011E+03  # ~c_R
   1000005     1.50000000E+02  # ~b_1
   2000005     2.00000000E+03  # ~b_2
   1000006     2.00000000E+02  # ~t_1
   2000006     2.00000000E+03  # ~t_2
   1000011     5.00808030E+03  # ~e_L-
   2000011     5.00346014E+03  # ~e_R-
   1000012     5.00707925E+03  # ~nu_eL
   1000013     5.00808290E+03  # ~mu_L-
   2000013     5.00346519E+03  # ~mu_R-
   1000014     5.00708182E+03  # ~nu_muL
   1000015     5.00488759E+03  # ~tau_1-
   2000015     5.00881777E+03  # ~tau_2-
   1000016     5.00780659E+03  # ~nu_tauL
   1000021     2.00000000E+03  # ~g
   1000022     9.48909737E+01  # ~chi_10
   1000023    -1.02918203E+02  # ~chi_20
   1000025     1.00000000E+03  # ~chi_30
   1000035     1.00802723E+03  # ~chi_40
   1000024     9.82221329E+01  # ~chi_1+
   1000037     1.00610672E+03  # ~chi_2+
# Higgs mixing
Block alpha # Effective Higgs mixing angle
          -1.05767632E-01   # alpha
Block Hmix Q=  7.67747601E+02  # Higgs mixing parameters
   1    1.00000000E+02  # mu
   2    9.64989384E+00  # tan[beta](Q)
   3    2.43811690E+02  # v(Q)
   4    9.79597680E+05  # m^2_A(Q)
Block stopmix # stop mixing matrix
   1  1    -7.07106781E-01   # Re[R_st(1,1)]
   1  2     7.07106781E-01   # Re[R_st(1,2)]
   2  1    -7.07106781E-01   # Re[R_st(2,1)]
   2  2    -7.07106781E-01   # Re[R_st(2,2)]
Block sbotmix # sbottom mixing matrix
   1  1     9.99847695E-01   # Re[R_sb(1,1)]
   1  2     1.74524064E-02   # Re[R_sb(1,2)]
   2  1    -1.74524064E-02   # Re[R_sb(2,1)]
   2  2     9.99847695E-01   # Re[R_sb(2,2)]
Block staumix # stau mixing matrix
   1  1     4.14615774E-02   # Re[R_sta(1,1)]
   1  2     9.99140099E-01   # Re[R_sta(1,2)]
   2  1    -9.99140099E-01   # Re[R_sta(2,1)]
   2  2     4.14615774E-02   # Re[R_sta(2,2)]
Block Nmix # neutralino mixing matrix
   1  1    -3.65910108E-02   # Re[N(1,1)]
   1  2     6.46033143E-02   # Re[N(1,2)]
   1  3    -7.20114004E-01   # Re[N(1,3)]
   1  4     6.89871968E-01   # Re[N(1,4)]
   2  1    -2.55828640E-02   # Re[N(2,1)]
   2  2     4.51678641E-02   # Re[N(2,2)]
   2  3     6.93634249E-01   # Re[N(2,3)]
   2  4     7.18454529E-01   # Re[N(2,4)]
   3  1     8.70123464E-01   # Re[N(3,1)]
   3  2     4.92833803E-01   # Re[N(3,2)]
   3  3     2.35235338E-16   # Re[N(3,3)]
   3  4    -1.01403867E-15   # Re[N(3,4)]
   4  1     4.90807266E-01   # Re[N(4,1)]
   4  2    -8.66545508E-01   # Re[N(4,2)]
   4  3    -1.75314206E-02   # Re[N(4,3)]
   4  4     8.88805854E-02   # Re[N(4,4)]
Block Umix # chargino mixing matrix
   1  1    -2.16563368E-02   # Re[U(1,1)]
   1  2     9.99765474E-01   # Re[U(1,2)]
   2  1     9.99765474E-01   # Re[U(2,1)]
   2  2     2.16563368E-02   # Re[U(2,2)]
Block Vmix # chargino mixing matrix
   1  1    -1.10006496E-01   # Re[V(1,1)]
   1  2     9.93930868E-01   # Re[V(1,2)]
   2  1     9.93930868E-01   # Re[V(2,1)]
   2  2     1.10006496E-01   # Re[V(2,2)]
DECAY        23     2.49520000E+00   # Z
DECAY        24     2.08500000E+00   # W
DECAY   1000011     2.37187861E+01   # ~e^-_L
#    BR                NDA      ID1      ID2
     1.40472927E-03    2     1000022        11   # BR(~e^-_L -> chi^0_1 e^-)
     6.54491655E-04    2     1000023        11   # BR(~e^-_L -> chi^0_2 e^-)
     9.48880250E-01    2     1000025        11   # BR(~e^-_L -> chi^0_3 e^-)
     4.90605253E-02    2     1000035        11   # BR(~e^-_L -> chi^0_4 e^-)
DECAY   2000011     6.12081545E+01   # ~e^-_R
#    BR                NDA      ID1      ID2
     6.46538705E-04    2     1000022        11   # BR(~e^-_R -> chi^0_1 e^-)
     3.17188908E-04    2     1000023        11   # BR(~e^-_R -> chi^0_2 e^-)
     1.82348779E-01    2     1000025        11   # BR(~e^-_R -> chi^0_3 e^-)
     2.15003851E-01    2     1000035        11   # BR(~e^-_R -> chi^0_4 e^-)
     2.99449503E-04    2    -1000024        12   # BR(~e^-_R -> chi^-_1 nu_e)
     6.01384193E-01    2    -1000037        12   # BR(~e^-_R -> chi^-_2 nu_e)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000013     2.37259798E+01   # ~mu^-_L
#    BR                NDA      ID1      ID2
     1.48282137E-03    2     1000022        13   # BR(~mu^-_L -> chi^0_1 mu^-)
     7.24107626E-04    2     1000023        13   # BR(~mu^-_L -> chi^0_2 mu^-)
     9.48591535E-01    2     1000025        13   # BR(~mu^-_L -> chi^0_3 mu^-)
     4.90471750E-02    2     1000035        13   # BR(~mu^-_L -> chi^0_4 mu^-)
     1.48795595E-04    2    -1000024        14   # BR(~mu^-_L -> chi^-_1 nu_mu)
DECAY   2000013     6.12115241E+01   # ~mu^-_R
#    BR                NDA      ID1      ID2
     6.74751013E-04    2     1000022        13   # BR(~mu^-_R -> chi^0_1 mu^-)
     3.45747308E-04    2     1000023        13   # BR(~mu^-_R -> chi^0_2 mu^-)
     1.82339686E-01    2     1000025        13   # BR(~mu^-_R -> chi^0_3 mu^-)
     2.14991609E-01    2     1000035        13   # BR(~mu^-_R -> chi^0_4 mu^-)
     2.98884116E-04    2    -1000024        14   # BR(~mu^-_R -> chi^-_1 nu_mu)
     6.01349323E-01    2    -1000037        14   # BR(~mu^-_R -> chi^-_2 nu_mu)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000015     2.57699044E+01   # ~tau^-_1
#    BR                NDA      ID1      ID2
     2.18703690E-02    2     1000022        15   # BR(~tau^-_1 -> chi^0_1 tau^-)
     1.86940399E-02    2     1000023        15   # BR(~tau^-_1 -> chi^0_2 tau^-)
     8.72876984E-01    2     1000025        15   # BR(~tau^-_1 -> chi^0_3 tau^-)
     4.57394329E-02    2     1000035        15   # BR(~tau^-_1 -> chi^0_4 tau^-)
     3.87452931E-02    2    -1000024        16   # BR(~tau^-_1 -> chi^-_1 nu_tau)
     2.07388088E-03    2    -1000037        16   # BR(~tau^-_1 -> chi^-_2 nu_tau)
DECAY   2000015     6.21445469E+01   # ~tau^-_2
#    BR                NDA      ID1      ID2
     8.45083276E-03    2     1000022        15   # BR(~tau^-_2 -> chi^0_1 tau^-)
     8.30883882E-03    2     1000023        15   # BR(~tau^-_2 -> chi^0_2 tau^-)
     1.79952887E-01    2     1000025        15   # BR(~tau^-_2 -> chi^0_3 tau^-)
     2.11573475E-01    2     1000035        15   # BR(~tau^-_2 -> chi^0_4 tau^-)
     1.42271112E-04    2    -1000024        16   # BR(~tau^-_2 -> chi^-_1 nu_tau)
     5.91571695E-01    2    -1000037        16   # BR(~tau^-_2 -> chi^-_2 nu_tau)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000012     6.12005364E+01   # ~nu_e
#    BR                NDA      ID1      ID2
     2.37824768E-03    2     1000022        12   # BR(~nu_e -> chi^0_1 nu_e)
     1.13847976E-03    2     1000023        12   # BR(~nu_e -> chi^0_2 nu_e)
     3.22845257E-02    2     1000025        12   # BR(~nu_e -> chi^0_3 nu_e)
     3.61933057E-01    2     1000035        12   # BR(~nu_e -> chi^0_4 nu_e)
     7.90263923E-03    2     1000024        11   # BR(~nu_e -> chi^+_1 e^-)
     5.94363051E-01    2     1000037        11   # BR(~nu_e -> chi^+_2 e^-)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000014     6.12040743E+01   # ~nu_mu
#    BR                NDA      ID1      ID2
     2.37811144E-03    2     1000022        14   # BR(~nu_mu -> chi^0_1 nu_mu)
     1.13841454E-03    2     1000023        14   # BR(~nu_mu -> chi^0_2 nu_mu)
     3.22826790E-02    2     1000025        14   # BR(~nu_mu -> chi^0_3 nu_mu)
     3.61912356E-01    2     1000035        14   # BR(~nu_mu -> chi^0_4 nu_mu)
     7.95935787E-03    2     1000024        13   # BR(~nu_mu -> chi^+_1 mu^-)
     5.94329081E-01    2     1000037        13   # BR(~nu_mu -> chi^+_2 mu^-)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000016     6.22010221E+01   # ~nu_tau
#    BR                NDA      ID1      ID2
     2.34033470E-03    2     1000022        16   # BR(~nu_tau -> chi^0_1 nu_tau)
     1.12033064E-03    2     1000023        16   # BR(~nu_tau -> chi^0_2 nu_tau)
     3.17706552E-02    2     1000025        16   # BR(~nu_tau -> chi^0_3 nu_tau)
     3.56172741E-01    2     1000035        16   # BR(~nu_tau -> chi^0_4 nu_tau)
     2.36853550E-02    2     1000024        15   # BR(~nu_tau -> chi^+_1 tau^-)
     5.84910584E-01    2     1000037        15   # BR(~nu_tau -> chi^+_2 tau^-)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000001     2.69457836E+02   # ~d_L
#    BR                NDA      ID1      ID2
     9.37065312E-03    2     1000025         1   # BR(~d_L -> chi^0_3 d)
     4.84538370E-04    2     1000035         1   # BR(~d_L -> chi^0_4 d)
     9.90124487E-01    2     1000021         1   # BR(~d_L -> ~g d)
DECAY   2000001     3.23930756E+02   # ~d_R
#    BR                NDA      ID1      ID2
     3.20392752E-04    2     1000022         1   # BR(~d_R -> chi^0_1 d)
     1.54159343E-04    2     1000023         1   # BR(~d_R -> chi^0_2 d)
     5.89782092E-02    2     1000035         1   # BR(~d_R -> chi^0_4 d)
     1.14876199E-01    2    -1000037         2   # BR(~d_R -> chi^-_2 u)
     8.25516823E-01    2     1000021         1   # BR(~d_R -> ~g d)
DECAY   1000003     2.69459637E+02   # ~s_L
#    BR                NDA      ID1      ID2
     9.37059186E-03    2     1000025         3   # BR(~s_L -> chi^0_3 s)
     4.84539243E-04    2     1000035         3   # BR(~s_L -> chi^0_4 s)
     9.90118208E-01    2     1000021         3   # BR(~s_L -> ~g s)
DECAY   2000003     3.23933068E+02   # ~s_R
#    BR                NDA      ID1      ID2
     3.21723112E-04    2     1000022         3   # BR(~s_R -> chi^0_1 s)
     1.55450411E-04    2     1000023         3   # BR(~s_R -> chi^0_2 s)
     5.89778118E-02    2     1000035         3   # BR(~s_R -> chi^0_4 s)
     1.14875466E-01    2    -1000037         4   # BR(~s_R -> chi^-_2 c)
     8.25511294E-01    2     1000021         3   # BR(~s_R -> ~g s)
DECAY   1000005     2.76916058E-02   # ~b_1
#    BR                NDA      ID1      ID2
     5.91451607E-01    2     1000022         5   # BR(~b_1 -> chi^0_1 b)
     4.08548393E-01    2     1000023         5   # BR(~b_1 -> chi^0_2 b)
DECAY   2000005     4.37221342E+00   # ~b_2
#    BR                NDA      ID1      ID2
     1.31287064E-01    2     1000022         5   # BR(~b_2 -> chi^0_1 b)
     1.23903203E-01    2     1000023         5   # BR(~b_2 -> chi^0_2 b)
     1.11740282E-01    2     1000025         5   # BR(~b_2 -> chi^0_3 b)
     3.57878080E-02    2     1000035         5   # BR(~b_2 -> chi^0_4 b)
     2.53716045E-01    2    -1000024         6   # BR(~b_2 -> chi^-_1 t)
     1.12457726E-03    2    -1000037         6   # BR(~b_2 -> chi^-_2 t)
     1.67733569E-01    2     1000006       -24   # BR(~b_2 -> ~t_1 W^-)
     4.33724027E-04    2     1000006       -37   # BR(~b_2 -> ~t_1 H^-)
     1.73934584E-01    2     1000005        23   # BR(~b_2 -> ~b_1 Z)
     3.33809803E-04    2     1000005        25   # BR(~b_2 -> ~b_1 h^0)
DECAY   1000002     2.77518700E+02   # ~u_L
#    BR                NDA      ID1      ID2
     3.64036281E-02    2     1000025         2   # BR(~u_L -> chi^0_3 u)
     1.88236571E-03    2     1000035         2   # BR(~u_L -> chi^0_4 u)
     9.61635119E-01    2     1000021         2   # BR(~u_L -> ~g u)
DECAY   2000002     3.23944406E+02   # ~u_R
#    BR                NDA      ID1      ID2
     2.10269673E-04    2     1000022         2   # BR(~u_R -> chi^0_1 u)
     1.01929736E-04    2     1000023         2   # BR(~u_R -> chi^0_2 u)
     9.64959658E-03    2     1000025         2   # BR(~u_R -> chi^0_3 u)
     4.96126316E-02    2     1000035         2   # BR(~u_R -> chi^0_4 u)
     1.50696742E-03    2     1000024         1   # BR(~u_R -> chi^+_1 d)
     1.13530554E-01    2     1000037         1   # BR(~u_R -> chi^+_2 d)
     8.25388051E-01    2     1000021         2   # BR(~u_R -> ~g u)
DECAY   1000004     2.77521426E+02   # ~c_L
#    BR                NDA      ID1      ID2
     3.64032778E-02    2     1000025         4   # BR(~c_L -> chi^0_3 c)
     1.88248959E-03    2     1000035         4   # BR(~c_L -> chi^0_4 c)
     9.61625871E-01    2     1000021         4   # BR(~c_L -> ~g c)
DECAY   2000004     3.23946722E+02   # ~c_R
#    BR                NDA      ID1      ID2
     2.12283420E-04    2     1000022         4   # BR(~c_R -> chi^0_1 c)
     1.04052497E-04    2     1000023         4   # BR(~c_R -> chi^0_2 c)
     9.64954103E-03    2     1000025         4   # BR(~c_R -> chi^0_3 c)
     4.96122338E-02    2     1000035         4   # BR(~c_R -> chi^0_4 c)
     1.50970295E-03    2     1000024         3   # BR(~c_R -> chi^+_1 s)
     1.13529623E-01    2     1000037         3   # BR(~c_R -> chi^+_2 s)
     8.25382563E-01    2     1000021         4   # BR(~c_R -> ~g c)
DECAY   1000006     1.05119132E+00   # ~t_1
#    BR                NDA      ID1      ID2
     1.00000000E+00    2     1000024         5   # BR(~t_1 -> chi^+_1 b)
#    BR                NDA      ID1      ID2       ID3
DECAY   2000006     1.12191961E+02   # ~t_2
#    BR                NDA      ID1      ID2
     8.35596906E-02    2     1000022         6   # BR(~t_2 -> chi^0_1 t)
     8.98571777E-02    2     1000023         6   # BR(~t_2 -> chi^0_2 t)
     1.32808481E-03    2     1000025         6   # BR(~t_2 -> chi^0_3 t)
     1.71330515E-01    2     1000024         5   # BR(~t_2 -> chi^+_1 b)
     3.24506521E-01    2     1000005        24   # BR(~t_2 -> ~b_1 W^+)
     1.80684909E-01    2     1000006        23   # BR(~t_2 -> ~t_1 Z)
     1.48676036E-01    2     1000006        25   # BR(~t_2 -> ~t_1 h^0)
DECAY   1000024     3.48060378E-10   # chi^+_1
#    BR                NDA      ID1      ID2       ID3
     4.46382574E-01    3     1000022        -1         2   # BR(chi^+_1 -> chi^0_1 d_bar u)
     2.20687141E-01    3     1000022        -3         4   # BR(chi^+_1 -> chi^0_1 s_bar c)
     1.48794277E-01    3     1000022       -11        12   # BR(chi^+_1 -> chi^0_1 e^+ nu_e)
     1.48050011E-01    3     1000022       -13        14   # BR(chi^+_1 -> chi^0_1 mu^+ nu_mu)
     3.60859974E-02    3     1000022       -15        16   # BR(chi^+_1 -> chi^0_1 tau^+ nu_tau)
DECAY   1000037     2.62309395E+01   # chi^+_2
#    BR                NDA      ID1      ID2
     2.92092536E-01    2     1000006        -5   # BR(chi^+_2 -> ~t_1 b_bar)
     4.01729919E-01    2    -1000005         6   # BR(chi^+_2 -> ~b^*_1 t)
     7.01221364E-02    2     1000022        24   # BR(chi^+_2 -> chi^0_1 W^+)
     7.56844620E-02    2     1000023        24   # BR(chi^+_2 -> chi^0_2 W^+)
     7.47297501E-02    2     1000024        23   # BR(chi^+_2 -> chi^+_1 Z)
     7.67082588E-02    2     1000024        25   # BR(chi^+_2 -> chi^+_1 h^0)
#    BR                NDA      ID1      ID2       ID3
     3.40169420E-03    3     1000024         6        -6   # BR(chi^+_2 -> chi^+_1 t t_bar)
     3.00491636E-03    3     1000022        -5         6   # BR(chi^+_2 -> chi^0_1 b_bar t)
     2.49398864E-03    3     1000023        -5         6   # BR(chi^+_2 -> chi^0_2 b_bar t)
DECAY   1000022     0.00000000E+00   # chi^0_1
DECAY   1000023     2.91411983E-08   # chi^0_2
#    BR                NDA      ID1      ID2
     2.34010315E-02    2     1000022        22   # BR(chi^0_2 -> chi^0_1 photon)
#    BR                NDA      ID1      ID2       ID3
     1.18130145E-01    3     1000022         2        -2   # BR(chi^0_2 -> chi^0_1 u u_bar)
     9.96723513E-02    3     1000022         4        -4   # BR(chi^0_2 -> chi^0_1 c c_bar)
     1.53130160E-01    3     1000022         1        -1   # BR(chi^0_2 -> chi^0_1 d d_bar)
     1.52917183E-01    3     1000022         3        -3   # BR(chi^0_2 -> chi^0_1 s s_bar)
     3.50572068E-02    3     1000022        11       -11   # BR(chi^0_2 -> chi^0_1 e^- e^+)
     3.50268222E-02    3     1000022        13       -13   # BR(chi^0_2 -> chi^0_1 mu^- mu^+)
     2.53139620E-02    3     1000022        15       -15   # BR(chi^0_2 -> chi^0_1 tau^- tau^+)
     2.10173823E-01    3     1000022        12       -12   # BR(chi^0_2 -> chi^0_1 nu_e nu_bar_e)
     2.90139354E-02    3     1000024         1        -2   # BR(chi^0_2 -> chi^+_1 d u_bar)
     2.90139354E-02    3    -1000024        -1         2   # BR(chi^0_2 -> chi^-_1 d_bar u)
     2.03895987E-02    3     1000024         3        -4   # BR(chi^0_2 -> chi^+_1 s c_bar)
     2.03895987E-02    3    -1000024        -3         4   # BR(chi^0_2 -> chi^-_1 s_bar c)
     9.67131889E-03    3     1000024        11       -12   # BR(chi^0_2 -> chi^+_1 e^- nu_bar_e)
     9.67131889E-03    3    -1000024       -11        12   # BR(chi^0_2 -> chi^-_1 e^+ nu_e)
     9.64690032E-03    3     1000024        13       -14   # BR(chi^0_2 -> chi^+_1 mu^- nu_bar_mu)
     9.64690032E-03    3    -1000024       -13        14   # BR(chi^0_2 -> chi^-_1 mu^+ nu_mu)
     4.86690417E-03    3     1000024        15       -16   # BR(chi^0_2 -> chi^+_1 tau^- nu_bar_tau)
     4.86690417E-03    3    -1000024       -15        16   # BR(chi^0_2 -> chi^-_1 tau^+ nu_tau)
DECAY   1000025     1.15570753E+01   # chi^0_3
#    BR                NDA      ID1      ID2
     2.79499389E-01    2     1000006        -6   # BR(chi^0_3 -> ~t_1 t_bar)
     2.79499389E-01    2    -1000006         6   # BR(chi^0_3 -> ~t^*_1 t)
     5.38460993E-02    2     1000005        -5   # BR(chi^0_3 -> ~b_1 b_bar)
     5.38460993E-02    2    -1000005         5   # BR(chi^0_3 -> ~b^*_1 b)
     1.59732990E-01    2     1000024       -24   # BR(chi^0_3 -> chi^+_1 W^-)
     1.59732990E-01    2    -1000024        24   # BR(chi^0_3 -> chi^-_1 W^+)
#    BR                NDA      ID1      ID2       ID3
     3.62436018E-03    3     1000022         6        -6   # BR(chi^0_3 -> chi^0_1 t t_bar)
     1.70327866E-03    3     1000023         6        -6   # BR(chi^0_3 -> chi^0_2 t t_bar)
     4.20138816E-03    3     1000024         5        -6   # BR(chi^0_3 -> chi^+_1 b t_bar)
     4.20138816E-03    3    -1000024        -5         6   # BR(chi^0_3 -> chi^-_1 b_bar t)
DECAY   1000035     2.24333654E+01   # chi^0_4
#    BR                NDA      ID1      ID2
     1.10898615E-01    2     1000006        -6   # BR(chi^0_4 -> ~t_1 t_bar)
     1.10898615E-01    2    -1000006         6   # BR(chi^0_4 -> ~t^*_1 t)
     2.38440040E-01    2     1000005        -5   # BR(chi^0_4 -> ~b_1 b_bar)
     2.38440040E-01    2    -1000005         5   # BR(chi^0_4 -> ~b^*_1 b)
     3.09445453E-02    2     1000024       -24   # BR(chi^0_4 -> chi^+_1 W^-)
     3.09445453E-02    2    -1000024        24   # BR(chi^0_4 -> chi^-_1 W^+)
     3.38291808E-02    2     1000022        23   # BR(chi^0_4 -> chi^0_1 Z)
     8.12674424E-02    2     1000023        23   # BR(chi^0_4 -> chi^0_2 Z)
     7.78565417E-02    2     1000022        25   # BR(chi^0_4 -> chi^0_1 h^0)
     3.69382434E-02    2     1000023        25   # BR(chi^0_4 -> chi^0_2 h^0)
#    BR                NDA      ID1      ID2       ID3
     2.42901074E-03    3     1000022         6        -6   # BR(chi^0_4 -> chi^0_1 t t_bar)
     1.78152482E-03    3     1000023         6        -6   # BR(chi^0_4 -> chi^0_2 t t_bar)
     2.60338469E-03    3     1000024         5        -6   # BR(chi^0_4 -> chi^+_1 b t_bar)
     2.60338469E-03    3    -1000024        -5         6   # BR(chi^0_4 -> chi^-_1 b_bar t)
DECAY   1000021     9.63346539E+01   # ~g
#    BR                NDA      ID1      ID2
     2.64613972E-01    2     1000006        -6   # BR(~g -> ~t_1 t_bar)
     2.64613972E-01    2    -1000006         6   # BR(~g -> ~t^*_1 t)
     2.27458493E-01    2     1000005        -5   # BR(~g -> ~b_1 b_bar)
     2.27458493E-01    2    -1000005         5   # BR(~g -> ~b^*_1 b)
#    BR                NDA      ID1      ID2       ID3
     5.90186323E-03    3     1000022         6        -6   # BR(~g -> chi^0_1 t t_bar)
     3.63497761E-03    3     1000023         6        -6   # BR(~g -> chi^0_2 t t_bar)
     2.96415841E-03    3     1000024         5        -6   # BR(~g -> chi^+_1 b t_bar)
     2.96415841E-03    3    -1000024        -5         6   # BR(~g -> chi^-_1 b_bar t)
DECAY        25     3.43149454E-03   # h^0
#    BR                NDA      ID1      ID2
     2.97035302E-04    2          13       -13   # BR(h^0 -> mu^- mu^+)
     8.38285525E-02    2          15       -15   # BR(h^0 -> tau^- tau^+)
     2.75575456E-04    2           3        -3   # BR(h^0 -> s s_bar)
     5.29159288E-01    2           5        -5   # BR(h^0 -> b b_bar)
     3.58719091E-02    2           4        -4   # BR(h^0 -> c c_bar)
     8.90491349E-02    2          21        21   # BR(h^0 -> g g)
     3.36980021E-03    2          22        22   # BR(h^0 -> photon photon)
# writing decays into V V* as 3-body decays
#    BR                NDA      ID1      ID2       ID3
     1.15861592E-02    3          24        11        12   # BR(h^0 -> W^+ e^- nu_e)
     1.15861592E-02    3          24        13        14   # BR(h^0 -> W^+ mu^- nu_mu)
     1.15861592E-02    3          24        15        16   # BR(h^0 -> W^+ tau^- nu_tau)
     4.05515572E-02    3          24         1        -2   # BR(h^0 -> W^+ d u_bar)
     4.05515572E-02    3          24         3        -4   # BR(h^0 -> W^+ s c_bar)
     1.15861592E-02    3         -24       -11       -12   # BR(h^0 -> W^- e^+ nu_bar_e)
     1.15861592E-02    3         -24       -13       -14   # BR(h^0 -> W^- mu^+ nu_bar_mu)
     1.15861592E-02    3         -24       -15       -16   # BR(h^0 -> W^- tau^+ nu_bar_tau)
     4.05515572E-02    3         -24        -1         2   # BR(h^0 -> W^- d_bar u)
     4.05515572E-02    3         -24        -3         4   # BR(h^0 -> W^- s_bar c)
     9.24864089E-04    3          23        11       -11   # BR(h^0 -> Z e^- e^+)
     9.24864089E-04    3          23        13       -13   # BR(h^0 -> Z mu^- mu^+)
     7.92740648E-04    3          23        15       -15   # BR(h^0 -> Z tau^- tau^+)
     5.28493765E-03    3          23        12       -12   # BR(h^0 -> Z nu_e nu_bar_e)
     3.69945636E-03    3          23         1        -1   # BR(h^0 -> Z d d_bar)
     3.69945636E-03    3          23         3        -3   # BR(h^0 -> Z s s_bar)
     3.69945636E-03    3          23         5        -5   # BR(h^0 -> Z b b_bar)
     3.69945636E-03    3          23         2        -2   # BR(h^0 -> Z u u_bar)
     3.69945636E-03    3          23         4        -4   # BR(h^0 -> Z c c_bar)
DECAY        35     2.12842208E+00   # H^0
#    BR                NDA      ID1      ID2
     3.25143910E-04    2          13       -13   # BR(H^0 -> mu^- mu^+)
     9.19321764E-02    2          15       -15   # BR(H^0 -> tau^- tau^+)
     2.68705721E-04    2           3        -3   # BR(H^0 -> s s_bar)
     5.28037796E-01    2           5        -5   # BR(H^0 -> b b_bar)
     2.61991799E-01    2           6        -6   # BR(H^0 -> t t_bar)
     4.81090372E-02    2     1000006  -1000006   # BR(H^0 -> ~t_1 ~t^*_1)
     1.05109062E-02    2     1000022   1000022   # BR(H^0 -> chi^0_1 chi^0_1)
     1.42959196E-03    2     1000022   1000023   # BR(H^0 -> chi^0_1 chi^0_2)
     7.23044131E-03    2     1000023   1000023   # BR(H^0 -> chi^0_2 chi^0_2)
     4.04081748E-02    2     1000024  -1000024   # BR(H^0 -> chi^+_1 chi^-_1)
     2.66859025E-03    2          23        23   # BR(H^0 -> Z Z)
     5.13658871E-03    2          24       -24   # BR(H^0 -> W^+ W^-)
     1.87976519E-03    2          25        25   # BR(H^0 -> h^0 h^0)
DECAY        36     2.02055989E+00   # A^0
#    BR                NDA      ID1      ID2
     3.42893340E-04    2          13       -13   # BR(A^0 -> mu^- mu^+)
     9.69519241E-02    2          15       -15   # BR(A^0 -> tau^- tau^+)
     2.83373865E-04    2           3        -3   # BR(A^0 -> s s_bar)
     5.56866560E-01    2           5        -5   # BR(A^0 -> b b_bar)
     2.69249881E-01    2           6        -6   # BR(A^0 -> t t_bar)
     1.72498628E-02    2     1000022   1000022   # BR(A^0 -> chi^0_1 chi^0_1)
     5.22940538E-03    2     1000023   1000023   # BR(A^0 -> chi^0_2 chi^0_2)
     4.81633348E-02    2     1000024  -1000024   # BR(A^0 -> chi^+_1 chi^-_1)
     5.46019624E-03    2          25        23   # BR(A^0 -> h^0 Z)
     1.06393182E-04    2          21        21   # BR(A^0 -> g g)
DECAY        37     1.61505598E+00   # H^+
#    BR                NDA      ID1      ID2
     4.30429564E-04    2         -13        12   # BR(H^+ -> mu^+ nu_e)
     1.21702498E-01    2         -15        12   # BR(H^+ -> tau^+ nu_e)
     3.17009105E-04    2          -3         4   # BR(H^+ -> s_bar c)
     8.62843312E-01    2          -5         6   # BR(H^+ -> b_bar t)
     1.30679824E-03    2     1000024   1000022   # BR(H^+ -> chi^+_1 chi^0_1)
     6.62444099E-03    2     1000024   1000023   # BR(H^+ -> chi^+_1 chi^0_2)
     6.77479400E-03    2          25        24   # BR(H^+ -> h^0 W^+)
DECAY         6     2.43000000E+00   # top
#    BR                NDA      ID1      ID2
     1.00000000E+00    2           5        24   # BR(t -> b W)
Block HiggsBoundsInputHiggsCouplingsFermions
# ScalarNormEffCoupSq PseudoSNormEffCoupSq NP IP1 IP2 IP2
    1.12565908E+00    0.00000000E+00        3  25   5   5  # h0-b-b eff. coupling^2, normalised to SM
    9.98743409E+01    0.00000000E+00        3  35   5   5  # H0-b-b eff. coupling^2, normalised to SM
    0.00000000E+00    1.00000000E+02        3  36   5   5  # A0-b-b eff. coupling^2, normalised to SM
#
    9.98743409E-01    0.00000000E+00        3  25   6   6  # h0-t-t eff. coupling^2, normalised to SM
    1.12565908E-02    0.00000000E+00        3  35   6   6  # H0-t-t eff. coupling^2, normalised to SM
    0.00000000E+00    1.00000000E-02        3  36   6   6  # A0-t-t eff. coupling^2, normalised to SM
#
    1.12565908E+00    0.00000000E+00        3  25  15  15  # h0-tau-tau eff. coupling^2, normalised to SM
    9.98743409E+01    0.00000000E+00        3  35  15  15  # H0-tau-tau eff. coupling^2, normalised to SM
    0.00000000E+00    1.00000000E+02        3  36  15  15  # A0-tau-tau eff. coupling^2, normalised to SM
#
Block HiggsBoundsInputHiggsCouplingsBosons
    9.99962803E-01        3  25  24  24  # h0-W-W eff. coupling^2, normalised to SM
    3.71970938E-05        3  35  24  24  # H0-W-W eff. coupling^2, normalised to SM
    0.00000000E+00        3  36  24  24  # A0-W-W eff. coupling^2, normalised to SM
#
    9.99962803E-01        3  25  23  23  # h0-Z-Z eff. coupling^2, normalised to SM
    3.71970938E-05        3  35  23  23  # H0-Z-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  36  23  23  # A0-Z-Z eff. coupling^2, normalised to SM
#
    1.10222292E+00        3  25  21  21  # h0-g-g eff. coupling^2, normalised to SM
    3.22772219E-03        3  35  21  21  # H0-g-g eff. coupling^2, normalised to SM
    1.24064663E-01        3  36  21  21  # A0-g-g eff. coupling^2, normalised to SM
#
    0.00000000E+00        3  25  25  23  # h0-h0-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  35  25  23  # H0-h0-Z eff. coupling^2, normalised to SM
    3.71970938E-05        3  36  25  23  # A0-h0-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  35  35  23  # H0-H0-Z eff. coupling^2, normalised to SM
    9.99962803E-01        3  36  35  23  # A0-H0-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  36  36  23  # A0-A0-Z eff. coupling^2, normalised to SM
#
    0.00000000E+00        4  25  21  21  23  # h0-g-g-Z eff. coupling^2, normalised to SM
    0.00000000E+00        4  35  21  21  23  # H0-g-g-Z eff. coupling^2, normalised to SM
    0.00000000E+00        4  36  21  21  23  # A0-g-g-Z eff. coupling^2, normalised to SM
Block SPhenoLowEnergy  # low energy observables
    1    0.00000000E+00   # BR(b -> s gamma)
    2    0.00000000E+00   # BR(b -> s mu+ mu-)
    3    0.00000000E+00   # BR(b -> s nu nu)
    4    0.00000000E+00   # BR(Bd -> mu+ mu-)
    5    0.00000000E+00   # BR(Bs -> mu+ mu-)
    6    0.00000000E+00   # BR(B_u -> tau nu)
    7    0.00000000E+00   # BR(B_u -> tau nu)/BR(B_u -> tau nu)_SM
    8    0.00000000E+00   # |Delta(M_Bd)| [ps^-1] 
    9    0.00000000E+00   # |Delta(M_Bs)| [ps^-1] 
   20    0.00000000E+00   # Delta(g-2)_electron/2
   21    0.00000000E+00   # Delta(g-2)_muon/2
   22    0.00000000E+00   # Delta(g-2)_tau/2
   23    0.00000000E+00   # electric dipole moment of the electron
   24    0.00000000E+00   # electric dipole moment of the muon
   25    0.00000000E+00   # electric dipole moment of the tau
   26    0.00000000E+00   # Br(mu -> e gamma)
   27    0.00000000E+00   # Br(tau -> e gamma)
   28    0.00000000E+00   # Br(tau -> mu gamma)
   29    0.00000000E+00   # Br(mu -> 3 e)
   30    0.00000000E+00   # Br(tau -> 3 e)
   31    0.00000000E+00   # Br(tau -> 3 mu)
   39    0.00000000E+00   # Delta(rho_parameter)
   40    0.00000000E+00   # BR(Z -> e mu)
   41    0.00000000E+00   # BR(Z -> e tau)
   42    0.00000000E+00   # BR(Z -> mu tau)
