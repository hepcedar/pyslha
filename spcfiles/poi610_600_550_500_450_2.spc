#=====================================================================
#  ISAJET SUSY parameters in Les Houches Accord format
#  Created by isa2lha.py (external script)
#             Version 1, March 12, 2010, b.k.gjelsten@fys.uio.no
#             The initial purpose of the script was to allow for output of 
#             isasusy.x to be interfaced to Prospino
#             (For bugs/feedback, please send an email)
#
#              The wig-file is the basis for the spectrum and mixing matrices
#              Optionally the out-file can be read as well (so far only implemented
#              for isasusy.x output), giving some extra info in the slha output
#              From the wig file only values at the low scale are available, 
#              hence 'General MSSM Simulation' is given as MODSEL
#===================================================================== SPINFO
Block SPINFO   # Program information
     1   From wig file of ISAJET        # Spectrum Calculator
#===================================================================== MODSEL
Block MODSEL   # Model selection
     1     0   # General MSSM Simulation
#===================================================================== SMINPUTS
Block SMINPUTS   # Standard Model inputs
     6     1.72500000e+02   # m_{top}(pole)
#===================================================================== MINPAR
Block MINPAR   # SUSY breaking input parameters
     3     4.00000000e+00   # tan(beta)
#===================================================================== MASS
Block MASS   # Scalar and gaugino mass spectrum
# PDG code     mass               particle
        25     1.05401000e+02   # h
        35     1.00780690e+03   # H
        36     1.00215120e+03   # H+
        37     1.00000000e+03   # A
   1000001     6.02577700e+02   # dL
   1000002     5.97885700e+02   # uL
   1000003     6.02577700e+02   # sL
   1000004     5.97887200e+02   # cL
   1000005     2.00013730e+03   # b1
   1000006     2.00409990e+03   # t1
   2000001     6.00472500e+02   # dR
   2000002     5.99054000e+02   # uR
   2000003     6.00472500e+02   # sR
   2000004     5.99055500e+02   # cR
   2000005     2.00078280e+03   # b2
   2000006     2.00447340e+03   # t2
   1000011     5.01961700e+02   # eL
   1000013     5.01961700e+02   # muL
   1000015     2.00040750e+03   # ta1
   2000011     2.00042530e+03   # eR
   2000013     2.00042530e+03   # muR
   2000015     2.00051060e+03   # ta2
   1000012     4.96319400e+02   # snue
   1000014     4.96319400e+02   # snum
   1000016     1.99908300e+03   # snut
   1000021     6.10000000e+02   # gl
   1000022     4.49950700e+02   # N1
   1000023     5.49834400e+02   # N2
   1000025    -9.01555400e+02   # N3
   1000035     9.16558100e+02   # N4
   1000024     5.49423000e+02   # C1
   1000037     9.14690700e+02   # C2
#===================================================================== NMIX
Block NMIX   # neutralino mixing matrix
  1  1     9.94416590e-01   # 
  1  2    -4.95728700e-02   # 
  1  3     7.75113300e-02   # 
  1  4    -5.16711100e-02   # 
  2  1    -6.64154400e-02   # 
  2  2    -9.79969200e-01   # 
  2  3     1.50425150e-01   # 
  2  4    -1.12346280e-01   # 
  3  1    -1.68200600e-02   # 
  3  2     2.83285700e-02   # 
  3  3     7.05705110e-01   # 
  3  4     7.07739290e-01   # 
  4  1    -8.02587900e-02   # 
  4  2     1.90789070e-01   # 
  4  3     6.88000440e-01   # 
  4  4    -6.95567190e-01   # 
#===================================================================== UMIX
Block UMIX   # chargino U mixing matrix
  1  1    -9.75486220e-01   # Umix_{11}
  1  2     2.20060440e-01   # Umix_{12}
  2  1    -2.20060440e-01   # Umix_{21}
  2  2    -9.75486220e-01   # Umix_{22}
#===================================================================== UMIX
Block VMIX   # chargino V mixing matrix
  1  1    -9.86365020e-01   # Vmix_{11}
  1  2     1.64572180e-01   # Vmix_{12}
  2  1    -1.64572180e-01   # Vmix_{21}
  2  2    -9.86365020e-01   # Vmix_{22}
#===================================================================== STOPMIX
Block STOPMIX   # stop mixing matrix
  1  1     7.69667948e-05   # O_{11}
  1  2    -9.99999997e-01   # O_{12}
  2  1     9.99999997e-01   # O_{21}
  2  2     7.69667948e-05   # O_{22}
#===================================================================== SBOTMIX
Block SBOTMIX   # sbottom mixing matrix
  1  1     4.36794897e-07   # O_{11}
  1  2     1.00000000e+00   # O_{12}
  2  1    -1.00000000e+00   # O_{21}
  2  2     4.36794897e-07   # O_{22}
#===================================================================== STAUMIX
Block STAUMIX   # stau mixing matrix
  1  1     2.33679490e-06   # O_{11}
  1  2     1.00000000e+00   # O_{12}
  2  1    -1.00000000e+00   # O_{21}
  2  2     2.33679490e-06   # O_{22}
#===================================================================== ALPHA
Block ALPHA   # Effective Higgs mixing parameter
          -2.49570180e-01   # alpha_h
#===================================================================== HMIX
Block HMIX (Q = )   # Higgs mixing parameters
     1     9.00000000e+02   # mu(Q)
     4     1.00000000e+06   # m_A^2(Q)
#===================================================================== AU
Block AU (Q = )   #
  1  1     2.25000000e+02   # A_u
  2  2     2.25000000e+02   # A_c
  3  3     2.25000000e+02   # A_t
#===================================================================== AD
Block AD (Q = )   #
  1  1     3.60000000e+03   # A_d
  2  2     3.60000000e+03   # A_s
  3  3     3.60000000e+03   # A_b
#===================================================================== AE
Block AE (Q = )   #
  1  1     3.60000000e+03   # A_e
  2  2     3.60000000e+03   # A_mu
  3  3     3.60000000e+03   # A_tau
